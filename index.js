(function() {

    "use strict";

    var transformTools = require("browserify-transform-tools");

    module.exports = transformTools.makeRequireTransform("requireTransform", { evaluateArguments: true },
        function(args, opts, cb) {
            if (args[0] === "jquery") {
                return cb(null, "window.jQuery || require('jquery')");
            } else {
                return cb();
            }
        });
})();